//! Initializes the I2C interface on
//! Pin1: SDA
//! Pin2: SCL
//! On the standard Espressif ESP32-C3 dev board.


#![no_std]
#![no_main]

use core::fmt::Write;

use esp32c3_hal::{pac::Peripherals, i2c::I2C, prelude::*, RtcCntl, Serial, Timer, IO};
use nb::block;
use panic_halt as _;
use riscv_rt::entry;

#[entry]
fn main() -> ! {
    let mut peripherals = Peripherals::take().unwrap();

    let mut rtc_cntl = RtcCntl::new(peripherals.RTC_CNTL);
    let mut serial0 = Serial::new(peripherals.UART0).unwrap();
    let mut timer0 = Timer::new(peripherals.TIMG0);
    let mut timer1 = Timer::new(peripherals.TIMG1);

    // Disable watchdog timers
    rtc_cntl.set_super_wdt_enable(false);
    rtc_cntl.set_wdt_enable(false);
    timer0.disable();
    timer1.disable();

    
    let io = IO::new(peripherals.GPIO, peripherals.IO_MUX);

    // Create a new peripheral object with the described wiring
    // and standard I2C clock speed
    let i2c = I2C::new(
        peripherals.I2C0,
        io.pins.gpio1,
        io.pins.gpio2,
        100_000,
        &mut peripherals.SYSTEM,
    ).unwrap();

    let mut driver = fs1027_dg_hal::Fs1027Dg::new_default(i2c);

    
    timer0.start(10_000u64);

    loop {
	match driver.try_read(){
	    Some(v) => {
		let _ = writeln!(serial0, "Read: {}", v);
	    },
	    None => {
		let _  = writeln!(serial0, "Failed to read!");
	    },
	}
	
	//Block 10 seconds
        block!(timer0.wait()).unwrap();
    }
}
