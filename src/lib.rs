//! # fs1027-dg-hal sensor driver
//!
//! The crate provides a basic sensor driver based on the I²C interface provided.
//!
//! The sensor's sheet can be found [here](https://www.renesas.com/us/en/document/dst/fs1027-dg-datasheet?language=en&r=1488711)
//!
//! Other versions of this sensor might work, but are not supported.

#![deny(unsafe_code, warnings)]
#![no_std]

extern crate embedded_hal as hal;
use core::marker::PhantomData;

use embedded_hal::blocking::i2c::Read;

///An instance of the driver that does not *own* the I²C bus.
pub struct Fs1027DgBusless<I2C: Read>{
    i2c: PhantomData<I2C>,
    addr: u8,
    read_buffer: [u8; 5],
}

///Driver definition that owns the I²C bus. Saver, since it can ensure that the
/// BUS is always the same.
pub struct Fs1027Dg<I2C: Read> {
    i2c: I2C,
    busless: Fs1027DgBusless<I2C>,
}

impl<I2C: Read> Fs1027DgBusless<I2C> {
    ///Creates the driver for the given I²C ports. Assumes that the I²C port is configured as master.
    ///
    /// Tries to initialize the driver on the given address, returns an error if this failed.
    ///
    /// If you are sure that the driver is at the given address and it still fails, make sure that you
    /// configured the SDA/SCL connection correctly. The recommend pull-up resistors are between 2.2k Ohm and 10k Ohm. Have a look at the [documentation](https://www.renesas.com/us/en/document/dst/fs1027-dg-datasheet?language=en&r=1488711) for further information.
    pub fn new(_i2c: &I2C, addr: u8) -> Self {
        let dev = Fs1027DgBusless {
            i2c: PhantomData,
            addr,
            read_buffer: [0; 5],
        };

        dev
    }

    ///Initializes the driver assuming the sensors address is the default one. If this fails, consider searching for the driver. Note that the driver only supports 7bit addresses.
    ///
    /// Have a look at [new](Self::new) for further documentation.
    pub fn new_default(i2c: &I2C) -> Self {
        Self::new(i2c, 0x50)
    }

    ///Reads a single 12bit value from the sensor. Returns 0 if the reading failed. Note that the default lowest value is 409 (per documentation).
    pub fn read(&mut self, i2c: &mut I2C) -> u16 {
        self.try_read(i2c).unwrap_or(0)
    }

    ///Tries to read a value, verifies it, and if this succeeds, returns the 12bit value.
    pub fn try_read(&mut self, i2c: &mut I2C) -> Option<u16> {
        //Per documentation: read 5 bytes, the first is the checksum, 1..2 is the data
        // and 3..4 is the generic checksum.
        //
        match i2c.read(self.addr, &mut self.read_buffer) {
            Ok(_) => {}
            Err(_e) => return None,
        }

        //Calculate validity
        let value = u16::from_be_bytes([self.read_buffer[1], self.read_buffer[2]]);
        let sum = self.read_buffer[1]
            .wrapping_add(self.read_buffer[2])
            .wrapping_add(self.read_buffer[3])
            .wrapping_add(self.read_buffer[4]);

        if self.read_buffer[0].wrapping_add(sum) == 0 {
            return Some(value);
        } else {
            None
        }
    }

    ///Reads the current flow rate in liters/min. Returns 0 if reading failed
    pub fn read_flow_rate(&mut self, i2c: &mut I2C) -> f32 {
        if let Some(rate) = self.try_read(i2c) {
            Self::bit_to_flow_rate(rate)
        } else {
            0.0
        }
    }

    ///Converts the raw bits to a flow rate in liters/min
    pub fn bit_to_flow_rate(bit: u16) -> f32 {
        ((bit - 409) as f32 / 33277.0) * 200.0
    }
}


impl<I2C: Read> Fs1027Dg<I2C>{
    ///Creates the driver for the given I²C ports. Assumes that the I²C port is configured as master.
    ///
    /// Tries to initialize the driver on the given address, returns an error if this failed.
    ///
    /// If you are sure that the driver is at the given address and it still fails, make sure that you
    /// configured the SDA/SCL connection correctly. The recommend pull-up resistors are between 2.2k Ohm and 10k Ohm. Have a look at the [documentation](https://www.renesas.com/us/en/document/dst/fs1027-dg-datasheet?language=en&r=1488711) for further information.
    pub fn new(i2c: I2C, addr: u8) -> Self {
        let busless = Fs1027DgBusless::new(&i2c, addr);
        let dev = Fs1027Dg {
            i2c,
            busless
        };

        dev
    }

    ///Initializes the driver assuming the sensors address is the default one. If this fails, consider searching for the driver. Note that the driver only supports 7bit addresses.
    ///
    /// Have a look at [new](Self::new) for further documentation.
    pub fn new_default(i2c: I2C) -> Self {
        Self::new(i2c, 0x50)
    }

    ///Reads a single 12bit value from the sensor. Returns 0 if the reading failed. Note that the default lowest value is 409 (per documentation).
    pub fn read(&mut self) -> u16 {
        self.busless.read(&mut self.i2c)
    }

    ///Tries to read a value, verifies it, and if this succeeds, returns the 12bit value.
    pub fn try_read(&mut self) -> Option<u16> {
        self.busless.try_read(&mut self.i2c)
    }

    ///Reads the current flow rate in liters/min. Returns 0 if reading failed
    pub fn read_flow_rate(&mut self) -> f32 {
        self.busless.read_flow_rate(&mut self.i2c)
    }

    ///Releases the I²C interface by consuming `self`.
    pub fn release(self) -> I2C {
        self.i2c
    }
}
